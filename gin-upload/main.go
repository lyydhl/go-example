package main

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path"
	"time"

	"github.com/gin-gonic/gin"
	nanoId "github.com/matoous/go-nanoid"
)

var (
	ErrUnsupportedFileFormat = errors.New("图片格式不支持")
	ErrFileStorePath         = errors.New("文件存储路径异常")
	ErrFileGenerateName      = errors.New("文件名称生成异常")
	ErrFileSaveFailed        = errors.New("文件保存失败")
)

func main() {
	r := gin.Default()
	// post
	// body form-data
	// name: files
	r.POST("/upload/single", singleUpload)
	r.POST("/upload/multipart", multipartUpload)
	r.Run()
}

func singleUpload(ctx *gin.Context) {
	f, err := ctx.FormFile("files")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	open, err := f.Open()
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	reader := bufio.NewReader(open)
	path, err := UploadImage(ctx, reader)
	if err == ErrUnsupportedFileFormat {
		ctx.JSON(http.StatusUnsupportedMediaType, ErrUnsupportedFileFormat.Error())
		return
	}
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, path)
}

type pictureResponse struct {
	ID  int    `json:"id"`
	URL string `json:"url"`
}

func multipartUpload(ctx *gin.Context) {
	form, err := ctx.MultipartForm()
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err.Error())
		return
	}
	fhs := form.File["files"]
	var prs []pictureResponse
	c := make(chan pictureResponse)
	defer close(c)
	for key, item := range fhs {

		go func(item *multipart.FileHeader, key int) {
			var res pictureResponse
			open, err := item.Open()
			if err != nil {
				ctx.JSON(http.StatusInternalServerError, err.Error())
				return
			}

			reader := bufio.NewReader(open)
			picture, err := UploadImage(ctx, reader)
			if err != nil {
				ctx.JSON(http.StatusInternalServerError, err.Error())
				return
			}
			res.ID = key + 1
			res.URL = picture
			c <- res
		}(item, key)
	}

	for {
		val, ok := <-c
		if ok {
			prs = append(prs, val)
		}
		if len(prs) >= len(fhs) {
			break
		}
	}

	ctx.JSON(http.StatusOK, prs)
}

func UploadImage(ctx context.Context, r io.Reader) (string, error) {
	img, format, err := image.Decode(r)
	if err != nil {
		return "", fmt.Errorf("读取图片错误")
	}
	if err == image.ErrFormat {
		return "", ErrUnsupportedFileFormat
	}
	if format != "png" && format != "jpeg" && format != "gif" {
		return "", ErrUnsupportedFileFormat
	}

	// 存放目录
	dir := path.Join("upload", time.Now().Format("20060102"))
	exist, _ := pathExists(dir)
	if !exist {
		err := mkDir(dir)
		if err != nil {
			return "", ErrFileStorePath
		}
	}

	filename, err := nanoId.Nanoid()
	if err != nil {
		return "", ErrFileGenerateName
	}

	path := path.Join("", dir, filename+"."+format)
	f, err := os.Create(path)
	if err != nil {
		return "", ErrFileStorePath
	}
	defer f.Close()

	var e error
	switch format {
	case "png":
		e = png.Encode(f, img)
	case "jpeg":
		e = jpeg.Encode(f, img, nil)
	case "gif":
		e = gif.Encode(f, img, nil)
	}
	if e != nil {
		return "", ErrFileSaveFailed
	}

	return "/" + path, nil
}

func pathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func mkDir(path string) error {
	return os.MkdirAll(path, os.ModePerm)
}
