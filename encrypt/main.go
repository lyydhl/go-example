package main

import (
	"log"

	"gitlab.com/go-example/encrypt/encrypt"
)

func main() {

	s := "123456"

	res, err := encrypt.AESEncrypt(s, encrypt.AES_KEY)
	if err != nil {
		log.Printf("aes encrypt error: %v", err)
	}
	log.Printf("encrypt: %s", res)

	log.Println(encrypt.Md5Fun01(s))
	log.Println(encrypt.Md5Fun02(s))
	log.Println(encrypt.Md5Fun03(s))
}
