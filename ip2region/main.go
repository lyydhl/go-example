package main

import (
	"log"
	"os"

	"gitlab.com/go-example/ip2region/ip2region"
)

func main() {
	path := "./ip2region.db"
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		log.Fatalf("not found db: %v", err)
	}

	region, err := ip2region.New(path)
	if err != nil {
		log.Fatalf("new ip2region err: %v", err)
	}
	defer region.Close()

	ipAddress, err := region.BtreeSearch("127.0.0.1")
	if err != nil {
		log.Printf("select ip address error: %v", err)
		return
	}
	log.Printf("ip address: %s", ipAddress)

	ipAddress, err = region.BtreeSearch("60.195.250.90")
	if err != nil {
		log.Printf("select ip address error: %v", err)
		return
	}
	log.Printf("ip address: %s", ipAddress)
}
