package db

import (
	"database/sql"

	mssql "github.com/denisenkom/go-mssqldb"
)

type MSSQLStore struct {
	DB *sql.DB
}

func NewMSSQLStore(db *sql.DB) *MSSQLStore {
	return &MSSQLStore{
		DB: db,
	}
}

func IsUniqueViolation(err error) bool {
	msErr, ok := err.(mssql.Error)
	return ok && msErr.Number == 2627
}

//func isForeignKeyViolation(err error) bool {
//	msErr, ok := err.(mssql.Error)
//	return ok && msErr.Number == 0
//}
