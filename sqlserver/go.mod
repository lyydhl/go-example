module gitlab.com/go-example/sqlserver

go 1.17

require (
	github.com/denisenkom/go-mssqldb v0.11.0
	github.com/google/uuid v1.3.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/golang-sql/civil v0.0.0-20190719163853-cb61b32ac6fe // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
