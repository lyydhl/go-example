package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/google/uuid"
	"gitlab.com/go-example/sqlserver/db"
	"gitlab.com/go-example/sqlserver/util"
)

func main() {
	conn, err := sql.Open("sqlserver", "server=127.0.0.1;user id=sa;password=123456;database=kenneth")
	if err != nil {
		log.Fatal("cannot connect to sql server:", err)
		return
	}

	db := db.NewMSSQLStore(conn)
	store := NewStore(db)

	ctx := context.Background()

	names := []string{"kenneth", "bonjour", "Math", "brands"}
	for i := 0; i < 20; i++ {
		for key, item := range names {
			err = store.Create(ctx, fmt.Sprintf("%s%d", item, i), item, key%2)
			if err != nil {
				log.Printf("%v", err)
			}
		}
	}

	err = store.Delete(ctx, 1)
	if err != nil {
		log.Printf("%v", err)
	}

	err = store.Update(ctx, 2, "z")
	if err != nil {
		log.Printf("%v", err)
	}

	u, err := store.Get(ctx, 10)
	if err != nil {
		log.Printf("%v", err)
	}
	log.Printf("get user: %#v", u)

	uu, err := store.List(ctx, 10, 10)
	if err != nil {
		log.Printf("%v", err)
	}
	for _, item := range uu {
		log.Printf("list user[%d]: %s", item.ID, item.Username)
	}

}

type Store struct {
	store *db.MSSQLStore
}

func NewStore(conn *db.MSSQLStore) *Store {
	return &Store{store: conn}
}

func (s *Store) Create(ctx context.Context, name string, password string, gender int) error {
	query := `INSERT INTO users(GUID,Username,Password,Gender) VALUES(@GUID,@Username,@Password,@Gender);`

	uid, err := uuid.NewRandom()
	if err != nil {
		return err
	}

	hashedPassword, err := util.HashPassword(password)
	if err != nil {
		return err
	}

	_, err = s.store.DB.ExecContext(ctx, query, sql.Named("GUID", uid.String()), sql.Named("Username", name), sql.Named("Password", hashedPassword), sql.Named("Gender", gender))
	if db.IsUniqueViolation(err) {
		return fmt.Errorf("用户名称已经占用了")
	}
	if err != nil {
		return err
	}

	return nil
}

func (s *Store) Delete(ctx context.Context, id int) error {
	query := `DELETE FROM users WHERE ID = @ID;`
	_, err := s.store.DB.ExecContext(ctx, query, sql.Named("ID", id))
	if err != nil {
		return err
	}

	return nil
}

func (s *Store) Update(ctx context.Context, id int, name string) error {
	query := `UPDATE users SET Username=@Name WHERE ID = @ID;`
	_, err := s.store.DB.ExecContext(ctx, query, sql.Named("Name", name), sql.Named("ID", id))
	if err != nil {
		return err
	}

	return nil
}

type User struct {
	ID          int
	GUID        string
	Username    string
	Password    string
	Gender      int
	CreatedTime time.Time
}

func (s *Store) Get(ctx context.Context, id int) (User, error) {
	query := `SELECT ID, CONVERT(VARCHAR(38),GUID) AS GUID, Username, Password, Gender, CreatedTime FROM users WHERE ID = @ID;`

	var u User
	if err := s.store.DB.QueryRowContext(ctx, query, sql.Named("ID", id)).
		Scan(&u.ID, &u.GUID, &u.Username, &u.Password, &u.Gender, &u.CreatedTime); err != nil {
		if err == sql.ErrNoRows {
			return u, fmt.Errorf("not found")
		}
		return u, err
	}

	return u, nil
}

func (s *Store) List(ctx context.Context, skip, limit int) ([]User, error) {
	query := `SELECT ID, CONVERT(VARCHAR(38),GUID) AS GUID, Username, Password, Gender, CreatedTime FROM users ORDER BY ID DESC OFFSET @Skip ROWS FETCH NEXT @Limit ROWS ONLY;`

	var uu []User
	rows, err := s.store.DB.QueryContext(ctx, query, sql.Named("Skip", skip), sql.Named("Limit", limit))
	if err != nil {
		return uu, err
	}
	defer rows.Close()

	for rows.Next() {
		var u User
		if err := rows.Scan(&u.ID, &u.GUID, &u.Username, &u.Password, &u.Gender, &u.CreatedTime); err != nil {
			return uu, err
		}

		uu = append(uu, u)
	}

	if err := rows.Close(); err != nil {
		return uu, err
	}

	if err := rows.Err(); err != nil {
		return uu, err
	}

	return uu, nil
}
