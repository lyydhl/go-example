package main

import (
	"log"
	"time"

	"gitlab.com/go-example/token/token"
)

func main() {
	key := "Z8SW7MTJlVxcLexq86L4Fnw54ZR7cc5j"
	jwtToken(key)
	pasetoToken(key)
}

func pasetoToken(key string) {
	pasetoToken, err := token.NewPasetoMaker(key)
	if err != nil {
		log.Printf("new paseto token error: %v", err)
		return
	}

	token, err := pasetoToken.CreateToken("kenneth", time.Hour*2)
	if err != nil {
		log.Printf("paseto token generate error: %v", err)
		return
	}
	log.Printf("paseto token: %s", token)

	user, err := pasetoToken.VerifyToken(token)
	if err != nil {
		log.Printf("paseto token verify error: %v", err)
		return
	}
	log.Printf("paseto user: %s", user)
}

func jwtToken(key string) {
	jwtToken, err := token.NewJWTMaker(key)
	if err != nil {
		log.Printf("new jwt token error: %v", err)
		return
	}

	token, err := jwtToken.CreateToken("kenneth", time.Hour*2)
	if err != nil {
		log.Printf("jwt token generate error: %v", err)
		return
	}
	log.Printf("jwt token: %s", token)

	user, err := jwtToken.VerifyToken(token)
	if err != nil {
		log.Printf("jwt token verify error: %v", err)
		return
	}
	log.Printf("jwt user: %s", user)
}
