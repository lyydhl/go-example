package main

import (
	"bufio"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path"
	"time"

	nanoId "github.com/matoous/go-nanoid"
)

var (
	// 图片最大数量
	maxImageSize             = 10 << 20
	ErrUnsupportedFileFormat = errors.New("图片格式不支持")
	ErrFileStorePath         = errors.New("文件存储路径异常")
	ErrFileGenerateName      = errors.New("文件名称生成异常")
	ErrFileSaveFailed        = errors.New("文件保存失败")
)

func main() {
	// post
	// body binary
	http.HandleFunc("/upload/single", singleUpload)
	// post
	// body form-data
	// name: files
	http.HandleFunc("/upload/single/form", singleFormUpload)
	http.HandleFunc("/upload/multipart", multipartUpload)
	http.ListenAndServe(":8080", nil)
}

func singleUpload(rw http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	ctx := r.Context()
	header := http.MaxBytesReader(rw, r.Body, int64(maxImageSize))
	defer header.Close()

	path, err := UploadImage(ctx, r.Body)
	if err == ErrUnsupportedFileFormat {
		rw.WriteHeader(http.StatusUnsupportedMediaType)
		rw.Write([]byte(ErrUnsupportedFileFormat.Error()))
		return
	}
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(err.Error()))
		return
	}

	rw.WriteHeader(http.StatusOK)
	rw.Write([]byte(path))
}

func singleFormUpload(rw http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	_, fh, err := r.FormFile("files")
	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		rw.Write([]byte(err.Error()))
		return
	}

	f, err := fh.Open()
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte("读取图片失败"))
		return
	}

	reader := bufio.NewReader(f)
	path, err := UploadImage(r.Context(), reader)
	if err == ErrUnsupportedFileFormat {
		rw.WriteHeader(http.StatusUnsupportedMediaType)
		rw.Write([]byte(ErrUnsupportedFileFormat.Error()))
		return
	}
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(err.Error()))
		return
	}

	rw.WriteHeader(http.StatusOK)
	rw.Write([]byte(path))
}

type imageResponse struct {
	ID  int    `json:"id"`
	URL string `json:"url"`
}

func multipartUpload(rw http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	r.ParseMultipartForm(int64(maxImageSize))
	files := r.MultipartForm.File["files"]

	var imgs []imageResponse
	c := make(chan imageResponse, 2)
	defer close(c)

	for i, file := range files {
		go func(item *multipart.FileHeader, key int) {
			var resp imageResponse

			f, err := item.Open()
			if err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				rw.Write([]byte("读取图片失败"))
				return
			}

			reader := bufio.NewReader(f)
			path, err := UploadImage(r.Context(), reader)
			if err == ErrUnsupportedFileFormat {
				rw.WriteHeader(http.StatusUnsupportedMediaType)
				rw.Write([]byte(ErrUnsupportedFileFormat.Error()))
				return
			}
			if err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				rw.Write([]byte(err.Error()))
				return
			}

			resp.ID = key + 1
			resp.URL = path
			c <- resp
		}(file, i)
	}

	for {
		v, ok := <-c
		if ok {
			imgs = append(imgs, v)
		}
		if len(files) == len(imgs) {
			break
		}
	}

	b, err := json.Marshal(imgs)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(fmt.Sprintf("could not marshal response: %v", err)))
		return
	}

	rw.WriteHeader(http.StatusOK)
	rw.Write([]byte(b))
}

func UploadImage(ctx context.Context, r io.Reader) (string, error) {
	img, format, err := image.Decode(r)
	if err != nil {
		return "", fmt.Errorf("读取图片错误")
	}
	if err == image.ErrFormat {
		return "", ErrUnsupportedFileFormat
	}
	if format != "png" && format != "jpeg" && format != "gif" {
		return "", ErrUnsupportedFileFormat
	}

	// 存放目录
	dir := path.Join("upload", time.Now().Format("20060102"))
	exist, _ := pathExists(dir)
	if !exist {
		err := mkDir(dir)
		if err != nil {
			return "", ErrFileStorePath
		}
	}

	filename, err := nanoId.Nanoid()
	if err != nil {
		return "", ErrFileGenerateName
	}

	path := path.Join("", dir, filename+"."+format)
	f, err := os.Create(path)
	if err != nil {
		return "", ErrFileStorePath
	}
	defer f.Close()

	var e error
	switch format {
	case "png":
		e = png.Encode(f, img)
	case "jpeg":
		e = jpeg.Encode(f, img, nil)
	case "gif":
		e = gif.Encode(f, img, nil)
	}
	if e != nil {
		return "", ErrFileSaveFailed
	}

	return "/" + path, nil
}

func pathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func mkDir(path string) error {
	return os.MkdirAll(path, os.ModePerm)
}
